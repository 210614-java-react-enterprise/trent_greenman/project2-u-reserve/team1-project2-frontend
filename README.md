# U-Reserve Frontend

This frontend for the U-Reserve project implements the React library.\
The documentation for React can be found here: [https://reactjs.org/docs/getting-started.html](https://reactjs.org/docs/getting-started.html)

## Components and Pages

Each file in the components folder represents a component that is shared across multiple pages. For example, the `mainNavigaion.js` file is used across the majority of our pages. Each file in the pages folder represents a page of our application. For example, `addtable.js` is the page that an admin would be brought to when they are adding tables to a restaurant which they just created. We control the routing to each of our pages in the `App.js` file and that is put into the `index.js` file.

## Styling
We contain all of the styling for our application in a file called `site.css` which can be found in the `src` folder. 

## External API
In the `reserve.js` file found in the `pages` folder, we use an external API called MailSlurp in order to send the user an email confirmation message once they have successfully made a reservation.\
The MailSlurp API can be found here: [https://www.mailslurp.com/](https://www.mailslurp.com/)
