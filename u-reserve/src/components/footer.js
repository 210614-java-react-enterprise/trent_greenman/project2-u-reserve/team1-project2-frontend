export default function Footer() {
    return (
        <div className="footer bg-dark mvh-45 d-flex flex-column pt-md-5 align-items-center overflow-auto">
            <div className="d-flex flex-column flex-md-row w-100 justify-content-center">
                {/* Columns */}
                <div className="card border-0 w-md-15 bg-transparent">
                    <div className="card-body d-flex flex-column">
                        <span className="card-title text-white text-uppercase mb-3">Reservation</span>
                        <a href="/restaurant" className="text-decoration-none text-white-80 text-small">Find Restaurants</a>
                        <a href="/reserve" className="text-decoration-none text-white-80 text-small">Reserve tables for events</a>
                        <a href="/business" className="text-decoration-none text-white-80 text-small"> Restaurant Register</a>
                        <a href="/adminlogin" className="text-decoration-none text-white-80 text-small">Administration</a>
                    </div>
                </div>
                <div className="card border-0 w-md-15 bg-transparent">
                    <div className="card-body d-flex flex-column">
                        <span className="card-title text-white text-uppercase mb-3">How It Works</span>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">How Booking for a Dinner Party Works</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">How Catering Works</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">How To help us list a restaurant</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">Protections and Guarantees</a>
                    </div>
                </div>
                <div className="card border-0 w-md-15 bg-transparent">
                    <div className="card-body d-flex flex-column">
                        <span className="card-title text-white text-uppercase mb-3">About U-Reserve</span>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">About Us</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">Customer Reviews</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">Careers</a>
                    </div>
                </div>
                <div className="card border-0 w-md-15 bg-transparent">
                    <div className="card-body d-flex flex-column">
                        <span className="card-title text-white text-uppercase mb-3">Support</span>
                        <a href="/contact" className="text-decoration-none text-white-80 text-small mb-2">Contact Us</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">FAQ</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">Business Partnerships</a>
                        <a href="/" className="text-decoration-none text-white-80 text-small mb-2">Media Relations</a>
                    </div>
                </div>
            </div>
            <div className="d-flex w-md-75 ps-md-3 flex-column">
                <div className="d-flex">
                    <a href="/" className="text-decoration-none text-white-80 text-small text-uppercase">Search</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/" className="text-decoration-none text-white-80 text-small text-uppercase">Restaurants</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/" className="text-decoration-none text-white-80 text-small text-uppercase">User Protections</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/" className="text-decoration-none text-white-80 text-small text-uppercase">Blog</a>
                </div>
                <div className="d-flex w-md-25 justify-content-between">
                    <a href="/"><i className="bi bi-facebook fs-1 text-white ms-2 ms-md-0"></i></a>
                    <a href="/"><i className="bi bi-twitter fs-1 text-white"></i></a>
                    <a href="/"><i className="bi bi-instagram fs-1 text-white"></i></a>
                    <a href="/"><i className="bi bi-youtube fs-1 text-white me-2 me-md-0"></i></a>
                </div>
            </div>
            
            <hr className="bg-white w-75" />
            
            <div className="d-flex flex-column my-3">
                <span className="text-white-80">Copyright &copy; 2021 U-Reserve. All Rights Reserved</span>
                <div className="d-flex">
                    <a href="/termsandconditions" className="text-decoration-none text-white-80 text-small text-capitalized">Terms and Conditions</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/privacyandpolicy" className="text-decoration-none text-white-80 text-small text-capitalized">Privacy Policy</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/businesscodeofethics" className="text-decoration-none text-white-80 text-small text-capitalized">Business Code of Ethics</a>
                    <span className="text-decoration-none text-white-80 text-small mx-2">|</span>
                    <a href="/" className="text-decoration-none text-white-80 text-small text-capitalized">Site Map</a>
                </div>
            </div>
        </div>
    );
}