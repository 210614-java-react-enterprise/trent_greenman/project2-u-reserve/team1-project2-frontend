
export default function HowItWorks() {
    return (
        <div className="d-flex flex-column justify-content-center align-items-center pt-3 shadow-full bg-white my-3 mx-1 pb-5">
            <span className="display-6 mx-auto">How it works</span>
            <div className="d-flex flex-column w-50 m-auto">
               {/* Step 1 */}
                <div className="d-flex w-75">
                    <div className="d-flex flex-column align-items-center justify-content-between">
                        <div className="stepcircle border border-1 d-flex rounded-circle align-items-center justify-content-center">
                            <span className="h6 m-0">1</span>
                        </div>
                        <div className="stepLine border flex-fill"></div> 
                    </div>
                    <div className="lead me-3 px-3 pt-2 flex-fill">
                    Search for restaurants by location or name
                    </div>
                    <i className="bi bi-search fs-1 mb-5"></i>
                </div>

                {/* Step 2 */}
                <div className="d-flex w-75">
                    <div className="d-flex flex-column align-items-center justify-content-between">
                        <div className="stepcircle border border-1 d-flex rounded-circle align-items-center justify-content-center">
                            <span className="h6 m-0">2</span>
                        </div>
                        <div className="stepLine border flex-fill"></div> 
                    </div>
                    <div className="lead me-3  px-3 pt-2 flex-fill">
                    Select the restaurant and choose a day, time, and party size.
                    </div>
                    <i className="bi bi-search fs-1 mb-5"></i>
                </div>

                {/* Step 3 */}
                <div className="d-flex w-75">
                    <div className="d-flex flex-column align-items-center justify-content-between">
                        <div className="stepcircle border border-1 d-flex rounded-circle align-items-center justify-content-center">
                            <span className="h6 m-0">3</span>
                        </div>
                        <div className="stepLine border flex-fill"></div> 
                    </div>
                    <div className="lead me-3  px-3 pt-2 flex-fill">
                    Select an open table that fits with your time and size constraints.
                    </div>
                    <i className="bi bi-search fs-1 mb-5"></i>
                </div>

                {/* Step 4 */}
                <div className="d-flex w-75">
                    <div className="d-flex flex-column align-items-center justify-content-between">
                        <div className="stepcircle border border-1 d-flex rounded-circle align-items-center justify-content-center">
                            <span className="h6 m-0">4</span>
                        </div>
                    </div>
                    <div className="lead me-3  px-3 pt-2 flex-fill">
                    Wait for an admin to approve your reservation request.Enjoy!
                    </div>
                    <i className="bi bi-search fs-1 mb-5"></i>
                </div>
            </div>
        </div>
    );
};