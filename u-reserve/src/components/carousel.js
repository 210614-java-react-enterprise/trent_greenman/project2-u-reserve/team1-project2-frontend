
export default function Carousel() {
    return (
        <div className={"stack carousel slide carousel-fade vh-80"} data-bs-ride="carousel">
            <div className={"stackitem carousel-inner h-100"}>
                <div className={"hero1 carousel-item h-100 active"} />
                <div className={"hero2 carousel-item h-100"} />
                <div className={"hero3 carousel-item h-100"} />
            </div>
            <div className={"stackitem bringtofront text-white"}>
                <div className="d-flex flex-column justify-content-center align-items-center h-100">
                    <div className="h5">It's All Here, All In One App</div>
                    <div className="p lead">Discover local on demand restaurant near and far. convenience by making reservations from location you perfer.</div>
                    <a className="btn btn-success calltoaction mt-3" href="/restaurant">Find Restaurants</a>
                </div>
            </div>
        </div>
    );
}