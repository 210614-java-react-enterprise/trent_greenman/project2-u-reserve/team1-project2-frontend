import React from 'react';

class MainNavigation extends React.Component {

    render() {
        return (
     <div className="container-fluid p-0">
        <div className="row">
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <a className="navbar-brand logo-text" href="/">U-Reserve</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="/">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link active" href="/restaurant">Reservation</a>
                            </li>
                            {/* <li className="nav-item">
                                <a className="nav-link active" href="/adminlogin">Business</a>
                            </li> */}
                            <li className="nav-item">
                                <a className="nav-link active" href="/contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
     </div> 
        );
    }
}

export default MainNavigation;