import finedining from '../images/finedining.jpg'
import topcuisine from '../images/topcuisine.jpg'
import experiences from '../images/experiences.jpg'

export default function Highlights() {
    return (
        <div className="d-flex flex-column flex-md-row justify-content-md-center pt-3 shadow-full bg-white my-3 mx-1 pb-5">
            <div className="appealcard border-0 mx-md-5 mb-3">
                <div className="card-body">
                    <div className="d-flex align-items-center">
                        <img src={finedining} alt="" className="rounded-circle highlightimage" />
                        <h6 className="mb-0 mt-1 ms-1 card-title">Fine Dining</h6>
                    </div>
                    <p className="card-text text-justify">Discover local fine dining restaurants and experience unique dishes from world class chefs. Enjoy a highly sophisticated restaurant experience for less by reserving a table with us as we match you up with restaurants offering generous deals</p>
                </div>
            </div>
            <div className="appealcard border-0  mx-md-5 mb-3">
                <div className="card-body">
                    <div className="d-flex align-items-center">
                        <img src={topcuisine} alt="" className="rounded-circle highlightimage" />
                        <h6 className="mb-0 mt-1 ms-1 card-title">Top Cuisines near you</h6>
                    </div>
                    <p className="card-text">From Sushi to Tacos, find regional delicacies near you from best contemporary American, Italian, Mexican, Greek, Mediteranean, African etc. restaurants</p>
                </div>
            </div>
            <div className="appealcard border-0 mx-md-5 mb-3">
                <div className="card-body">

                    <div className="d-flex align-items-center">
                        <img src={experiences} alt="" className="rounded-circle highlightimage" />
                        <h6 className="mb-0 mt-1 ms-1 card-title">Discover Experiences</h6>
                    </div>
                    <p className="card-text">Find out what delicious experiences are trending in your local area. Discover tasting menus, food tours, Saturday and Sunday brunches, wines and craft beers from local breweries.</p>
                </div>
            </div>
        </div>);
}