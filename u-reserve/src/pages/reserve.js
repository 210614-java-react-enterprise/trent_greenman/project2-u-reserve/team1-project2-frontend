import React, { useState, useEffect,useRef } from 'react';
import MainNavigation from '../components/mainNavigation';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import roundtable from '../images/round-table.png';
import MailSlurp from 'mailslurp-client';
import Footer from '../components/footer';

const mailslurp = new MailSlurp({ apiKey: "af09981b8affd4ed72c06191454258b498d6f3196de5bdf9fc25f57e5acd5cb3" });

export default function ReserveTables() {
    const [reservationDate, setReservationDate] = useState(new Date());
    const [reservationTime, setReservationTime] = useState(new Date().getHours());
    const [email, setEmail] = useState("");
    const [tables, setTables] = useState([]);
    const sessionRestaurant = sessionStorage.getItem("restaurant");
    const selectedTables = [];
    const dateChanged = useRef(true);
    let restaurant = null;
    if (sessionRestaurant) {
        restaurant = JSON.parse(sessionRestaurant);
    }

    function handleDateChange(newDate){
        setReservationDate(newDate);
        dateChanged.current = true;
        getTables();
    }

    async function getTables() {
        const restaurantId = restaurant && restaurant.id;
        const datetime = new Date(reservationDate.getFullYear(),reservationDate.getMonth(),reservationDate.getDate(),0,0,0,0,0).toISOString();
        let response = await fetch(`http://localhost:8081/table?restaurantid=${restaurantId}&datetime=${datetime}`, {
            headers: {
                'Authorization': 'Token'
            },
        });
        let data = await response.json();
        setTables(data);
    }
 
    useEffect(() => {
        if(dateChanged.current){
            getTables();
            dateChanged.current = false;
        }
        
    });
    return (
        <div className="container-fluid p-0">
            <MainNavigation />
            <div className="row h-100">
                <div className="col-12 justify-content-between">
                    <div className="d-flex flex-column align-items-center h-100 ">
                        <span className="h1 text-dark">Find your table for any occasion</span>
                        <div className="w-50 m-2">
                            <input className="form-control form-control-lg" type="text" placeholder="Enter email for response" value={email} onChange={(e) => setEmail(e.target.value)} />
                        </div>
                        <div className="d-flex justify-content-between p-0 w-50">
                            <div className="mb-3 input-group">
                                <span className="input-group-text bg-white border-end-0">
                                    <i className="bi bi-calendar"></i>
                                </span>
                                <DatePicker className="form-control border-start-0" selected={reservationDate} minDate={new Date()} selectsStart={true} onChange={(date) => handleDateChange(date)} />
                            </div>
                            <div className="mb-3 input-group">
                                <span className="input-group-text bg-white border-end-0"><i className="bi bi-clock"></i>
                                </span>
                                <select className="form-select border-start-0 m-0" onChange={(e) => setReservationTime(e.target.value)}>
                                    <option defaultValue="time">time</option>
                                    {
                                        getTimeOptions().map(option => (
                                            <option key={option.value} value={option.value}>{option.timeText}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between m-5 w-75 mx-auto">
                    {getTableImages(tables, selectedTables).map((table,index) => (
                        <div key={"restauranttable"+index} className="d-flex flex-column me-5 p-2">
                            {table}
                        </div>
                    ))}
                </div>
                <div className="d-flex justify-content-center">
                    <button className="btn btn-primary" onClick={() => reserveTable(email, reservationDate,reservationTime, selectedTables, restaurant.id)}>Reserve selected tables</button>
                </div>
            </div>
            <Footer />
        </div>
    );
}

function getTableImages(tables, selectedTables) {
    let tableImages = [];
    let key = 0;
    let tablesCount = tables.length;
    let currentTableIndex = 0;
    while (tablesCount > 0) {
        let columns = tablesCount > 3 ? 3 : tablesCount;
        let tableColumns = [];
        for (let j = 0; j < columns; j++) {
            let tableId = "table" + (tablesCount - key - (j + 1));
            tableColumns.push(getTableImage(tableId, tables[currentTableIndex], selectedTables));
            currentTableIndex++;
        }
        tableImages.push(tableColumns);
        tablesCount -= 3;
        key += 3;
    }
    return tableImages;
}

function getTableImage(tableId, table, selectedTables) {
    return (
        <div key={tableId} className="form-check mb-3">
            <input className="btn-check" type="checkbox" name="tables" id={tableId} onChange={(e) => addOrRemoveSelectedTable(selectedTables, table, e.target.checked)} disabled={table.status !== 'Available'} />
            <label className={"btn btn-" + (table.status === 'Available' ? 'outline-success' : 'secondary')} htmlFor={tableId}>
                <div className="d-flex flex-column">
                    <div className="d-flex flex-column justify-content-center">
                        <span>{table.status}</span>
                        <span>seats {table.capacity}</span>
                    </div>
                    <img className="tableimage img-fluid" src={roundtable} alt="tablePlace" />
                </div>
            </label>
        </div>
    )
}

function addOrRemoveSelectedTable(selectedTables, table, add) {
    if (add) {
        selectedTables.push(table);
    } else {
        const index = selectedTables.indexOf(table);
        if (index >= 0)
            selectedTables.splice(index, 1);
    }
}

function getTimeOptions() {
    let times = [];
    for (let i = 0; i < 24; i++) {
        let value = i;
        let timeText;

        if (i === 0)
            timeText = 12 + " AM";
        else if (i === 12)
            timeText = 12 + " PM";
        else if (i > 12)
            timeText = i - 12 + " PM";
        else
            timeText = i + " AM";

        times.push({ value: value, timeText: timeText });
    }
    return times;
}



//fetch where from backend to add to the database
async function reserveTable(email, reservationDate, reservationTime, selectedTables, restaurantId) {

    reservationDate.setHours(reservationTime);
    reservationDate.setMinutes(0);
    reservationDate.setSeconds(0);

    let reservations = selectedTables.map(table => {
        return {
            table: table,
            email: email,
            reservationDatetime: reservationDate,
            restaurant: {
                id: restaurantId
            }
        }
    });

    const response = await fetch('http://localhost:8081/reservation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'token'
        },
        body: JSON.stringify(reservations),
    });

    try{
        await mailslurp.inboxController.sendEmailAndConfirm(
            "d21809fe-f12b-4359-8a8e-686b419680b9",
            {
                to: [`${email}`],
                subject: "Reseveration receive",
                body:"Your Reservation is waiting for approval",
            },
        );
        if (response.status === 201){
            alert('reservation received');
            window.location.href="/";
        }
    }catch(err){
        window.location.href="/";
    }
    
}
