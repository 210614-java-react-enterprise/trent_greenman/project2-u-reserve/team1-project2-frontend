import React, { useState } from 'react';
import { MailSlurp } from "mailslurp-client";
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';
const mailslurp = new MailSlurp({ apiKey: "af09981b8affd4ed72c06191454258b498d6f3196de5bdf9fc25f57e5acd5cb3" });

export default function Contact() {
    const [username, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');


    return (
        <div className="container-fluid restaurant h-100 p-0">
                <MainNavigation />
                <div className="row">
                <div className="d-flex d-flex-column w-75 me-5 h-85 justify-content-between m-5 card">
                    <div className="card-body form-group text-dark">
                        <h4 className="card-title">Any Problem Contact Us</h4>
                        <div className="form-group">
                            <div className="form-group">
                                <label className="h5">Name</label>
                                <input type="text" id="fullname" className="form-control" value={username} onChange={(e) => setName(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label className="h5 mt-2">Email</label>
                                <input type="text" id="email" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label className="h5 mt-2">Messages</label>
                                <textarea className="form-control" id="messages" rows="3" value={message} onChange={(e) => setMessage(e.target.value)}></textarea>
                            </div>
                            <button type="submit" className="btn btn-secondary mt-3" onClick={(e) => handleSubmit(e, username, email, message, setEmail, setName, setMessage)}>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div >
    )
}

async function handleSubmit(event, username, email, message, setEmail, setName, setMessage) {
    event.preventDefault();

    let inbox = await mailslurp.getInbox('d21809fe-f12b-4359-8a8e-686b419680b9');
    const options = {
        to: ['d21809fe-f12b-4359-8a8e-686b419680b9@mailslurp.com'],
        subject: 'Hey i need you help',
        body: `Message from: ${username}.
                      
                  ${message}
                My email is: ${email}`
    }
    await mailslurp.sendEmail(inbox.id, options);
    setEmail('');
    setName('');
    setMessage('');
    alert('message received');
}