import React, { useState } from 'react';
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';


export default function Restaurant() {

    const [restaurants, setRestaurants] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");

    async function handleGO() {
        let response = await fetch(`http://localhost:8081/restaurant?searchTerm=${searchTerm}`, {
            headers: {
                'Authorization':'Token'
            }
        });
        let data = await response.json();
        setRestaurants(data);
    }

    function findTables(restaurant) {
        sessionStorage.setItem("restaurant", JSON.stringify(restaurant));
        window.location.href = "/reserve";
    };

    return (
        <div className="container-fluid p-0">
            <MainNavigation />
            <div className="row h-100">
                <div className="col-12 h-100 hero">
                    <div className="d-flex flex-column align-items-center h-100 justify-content-center">
                        <span className="h3 text-white">Let's Find You A Restaurant</span>
                        <div className="d-flex justify-content-center">
                            <div className="input-group mb-3">
                                <span className="input-group-text bg-white border-end-0"><i className="bi bi-search"></i></span>
                                <input type="text" className="form-control border-start-0" placeholder="name, location etc." value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
                                <button className="btn btn-primary" type="button" onClick={() => handleGO()}>Go</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex flex-column w-50 mx-auto mt-3 mvh-30">
                    {
                        restaurants && restaurants.map((restaurant, index) => (
                            <div key={"restaurant" + index} className="d-flex flex-column">
                                <div className="d-flex mb-3">
                                    <img src={restaurant.photo} className="img-thumbnail p-0 restaurantphoto me-3" alt="" />
                                    <div className="d-flex flex-column justify-content-between">
                                        <div className="d-flex flex-column">
                                            <span className="restaurantname h6">{restaurant.name}</span>
                                            <div className="d-flex">
                                                {getStars(restaurant.rating)}
                                                <i className="bi bi-dot"></i>
                                                <span>reviews</span>
                                            </div>
                                            <div className="d-flex align-items-center">
                                                <div className="d-flex">
                                                    {getPriceDollarSigns(restaurant.priceLevel)}
                                                </div>

                                                <i className="bi bi-dot"></i>
                                                <span className="ms-1 fw-bolder">
                                                    {getAddressStreet(restaurant.address)}
                                                </span>
                                            </div>
                                        </div>
                                        <div className="d-flex">
                                            <span className="">{restaurant.cuisine}</span>
                                         </div>
                                        <button onClick={() => findTables(restaurant)} className="btn btn-outline-primary btn-sm findtablebutton" type="button">Find table</button>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        ))}
                </div>
            </div>
            <Footer />
        </div>
    );
}

function getStars(rating) {
    let stars = [];
    let currentRating = rating;
    for (let i = 1; i <= 5; i++) {
        if (currentRating > 1)
            stars.push(<i key={i} className="bi bi-star-fill ratingstar me-1"></i>)
        else if (currentRating > 0)
            stars.push(<i key={i} className="bi bi-star-half ratingstar me-1"></i>)
        else
            stars.push(<i key={i} className="bi bi-star ratingstar me-1"></i>)

        currentRating--;
    }
    return stars;
}

function getPriceDollarSigns(priceLevel) {
    let dollarSigns = [];
    let currentLevel = priceLevel;
    for (let i = 0; i < 4; i++) {
        if (currentLevel > 1)
            dollarSigns.push(<span key={i} className="me-1 fw-bolder">$</span>)
        else
            dollarSigns.push(<span key={i} className="me-1 fw-lighter text-muted">$</span>)

        currentLevel--;
    }
    return dollarSigns;
}

function getAddressStreet(address) {
    return address.split(',')[0];
}
