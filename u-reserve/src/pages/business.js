import React, { useState } from 'react';
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';

export default function Business() {

    const [restauntName, setRestaurantName] = useState("");
    const [address, setAddress] = useState("");
    const [cuisine, setCuisine] = useState("");
    const [photo, setPhoto] = useState("");
    const [priceLevel, setPriceLevel] = useState(1);

    async function handleSubmit(e) {
        console.log(e);
        e.preventDefault();
        const data = {
            name: restauntName,
            address: address,
            photo: photo,
            cuisine: cuisine,
            priceLevel:priceLevel
        };

        const response = await fetch('http://localhost:8081/restaurant', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token'
            },
            body: JSON.stringify(data),
        });
        const restaurant = await response.json();
        if (restaurant) {
            sessionStorage.setItem("restaurant", JSON.stringify(restaurant));
            window.location.href = "/addtable"
        }
    }
    return (
        <div className="container-fluid p-0">
            <div className="row h-100">
                <MainNavigation />
                <div className="card col-12 flex-fill d-flex w-75 flex-column justify-content-between m-5">
                    <span className="h3 text-center">Connect with us today</span>
                    <div className="d-flex mt-2 m-1 justify-content-center">
                        <p className="pe-5">Fill out the form below to contact our sales team, and to receive email marketing communications from U-Reserve about parternering with us</p>
                    </div>
                    <div className="d-flex w-50 mx-auto flex-column justify-content-between m-1">
                        <div className="mb-2">
                            <label className="form-label h6" htmlFor="restaurantName">Restaurant Name</label>
                            <input id="restaurantName" type="text" className="form-control" value={restauntName} onChange={(e) => setRestaurantName(e.target.value)} />
                        </div>
                        <div className="mb-2">
                            <label className="form-label h6" htmlFor="address">Address</label>
                            <input id="address" type="text" className="form-control" value={address} onChange={(e) => setAddress(e.target.value)} />
                        </div>
                        <div className="mb-2">
                            <label className="form-label h6" htmlFor="cuisine">Cuisine</label>
                            <input id="cuisine" type="text" className="form-control" value={cuisine} onChange={(e) => setCuisine(e.target.value)} />
                        </div>
                        <div className="mb-2">
                            <label className="form-label h6 me-2" htmlFor="priceLevelSelection">Price Level</label>
                            <div className="btn-group w-100" role="group" id="priceLevelSelection">
                                <input type="radio" className="btn-check" name="priceLevel" id="level1" autoComplete="off" checked={priceLevel===1} onChange={() => setPriceLevel(1)}/>
                                <label className="btn btn-outline-secondary" htmlFor="level1">$</label>
                                <input type="radio" className="btn-check" name="priceLevel" id="level2" autoComplete="off" checked={priceLevel===2} onChange={() => setPriceLevel(2)}/>
                                <label className="btn btn-outline-secondary" htmlFor="level2">$$</label>
                                <input type="radio" className="btn-check" name="priceLevel" id="level3" autoComplete="off" checked={priceLevel===3} onChange={() => setPriceLevel(3)}/>
                                <label className="btn btn-outline-secondary" htmlFor="level3">$$$</label>
                                <input type="radio" className="btn-check" name="priceLevel" id="level4" autoComplete="off" checked={priceLevel===4} onChange={() => setPriceLevel(4)} />
                                <label className="btn btn-outline-secondary" htmlFor="level4">$$$$</label>
                            </div>
                        </div>
                        <div className="mb-2">
                            <label className="form-label" htmlFor="photo">
                                <span className="h6">Logo Url</span>
                                <span className="fw-lighter text-muted">(example: http://www.example.com/logo.png)</span>
                            </label>
                            <input id="photo" type="text" className="form-control" value={photo} onChange={(e) => setPhoto(e.target.value)} />
                        </div>
                        <div className="mb-2">
                            <button className="btn btn-primary" type="submit" onClick={(e) => handleSubmit(e)}>Register</button>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}
