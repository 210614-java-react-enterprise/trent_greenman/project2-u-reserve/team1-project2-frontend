import React, { useState } from 'react';
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';

export default function AddTable() {

    const [tables, setTables] = useState([]);
    const [capacity, setCapacity] = useState("");
    const [tableVisible, setTableVisible] = useState("d-none");
   
    function handleAddTable() {
        let newTables = [...tables];
        newTables.push(capacity);
        setTables(newTables);
        setCapacity("");
        const sessionRestaurant = sessionStorage.getItem("restaurant");
        let restaurant = null;
        if (sessionRestaurant) {
            restaurant = JSON.parse(sessionRestaurant);
        }
        const data = {
            capacity: capacity,
            restaurant: {
                id: restaurant && restaurant.id
            }
        };
    
        fetch('http://localhost:8081/table', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':'Token'
            },
            body: JSON.stringify(data),
        });
        if(tableVisible === 'd-none'){
            setTableVisible('');
        }
    }
    return (
        <div className="container-fluid p-0 h-100">
            <MainNavigation />

            <div className="card m-5 addtablecontainer">
                <div className="d-flex h-85 flex-fill flex-column">
                    <span className="h3 text-center m-4">Tell us about seating options in your restaurant</span>
                    <div className="input-group mb-3 w-50 mx-auto">
                        <label className="form-label h6 me-3" htmlFor="capacity">Capacity</label>
                        <input id="capacity" type="text" className="form-control" value={capacity} onChange={(e) => setCapacity(e.target.value)} />
                        <button className="btn btn-primary" type="button" onClick={() => handleAddTable()}>Add</button>
                    </div>
                    <table className={"table w-75 mx-auto " + tableVisible}>
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Capacity</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tables.map((table, index) =>
                                <tr key={"table" + index}>
                                    <th scope="row">Table {index + 1}</th>
                                    <td>{table}</td>
                                </tr>
                            )}

                        </tbody>
                    </table>
                </div>
            </div>
            <Footer />
        </div>
    );
}
