import React, { useState } from 'react';
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';


export default function AdministratorLogin() {
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');

    const data = {
        username: username,
        password: password
    };

    async function handleSubmit() {
        let response = await fetch(`http://localhost:8081/admin`, {
            method: 'POST',
            headers: {
                'Authorization': 'Token',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (response.status === 200) {
            const adminId = await response.text();
            sessionStorage.setItem('adminid', adminId);
            window.location.href = "/administration";
        } else {
            alert("log in failed");
        }
    }

    return (
        <div className="container-fliud h-100">
            <MainNavigation />
            <div className="shadow-full bg-white w-50 h-85 m-5 mx-auto p-5">
                <div className="d-flex flex-column m-5">
                    <span className="text-center h5">Administrator Log-in</span>
                    <div className="d-flex flex-column">
                        <div className="form-group">
                            <label className="text-dark h6" htmlFor="username">User name</label>
                            <input type="text" id="username" className="form-control" value={username} onChange={(e) => setUsername(e.target.value)} />
                        </div>
                        <div className="form-group mt-3 h6">
                            <label className="text-dark" htmlFor="password">Password</label>
                            <input type="password" id="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </div>
                        <div className="d-flex">
                        <button type="submit" className="btn btn-primary mt-4" onClick={(e) => handleSubmit(e)}>Log in</button>
                        </div>                    
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}