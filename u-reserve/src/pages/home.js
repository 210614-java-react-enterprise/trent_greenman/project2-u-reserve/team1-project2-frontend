import React from 'react';
import MainNavigation from '../components/mainNavigation';
import Carousel from '../components/carousel';
import Highlights from '../components/highlights';
import HowItWorks from '../components/howitworks';
import Footer from '../components/footer';

export default function  Home() {
        return (
            <div className="container-fluid p-0">
                <MainNavigation/>
                <Carousel/>
                <Highlights />
                <HowItWorks />
                <Footer />
        </div>
        );
    }

