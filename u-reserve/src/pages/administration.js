import React, { useState, useEffect, useRef } from 'react';
import MainNavigation from '../components/mainNavigation';
import Footer from '../components/footer';
import MailSlurp from 'mailslurp-client';
const mailslurp = new MailSlurp({ apiKey: "af09981b8affd4ed72c06191454258b498d6f3196de5bdf9fc25f57e5acd5cb3" });

export default function Administration() {

    const [reservations, setReservations] = useState([]);
    const adminId = sessionStorage.getItem("adminid");
    const reservationsLoaded = useRef(false);
    async function getReservations() {
        let response = await fetch(`http://localhost:8081/reservation?adminid=${adminId}`, {
            headers: {
                'Authorization': 'Token'
            },
        });
        let data = await response.json();
        refreshReservations(data);

    }

    function refreshReservations(data) {
        const reservations = [];
        for (const reservation of data) {
            const existing = reservations.find(r => r.email === reservation.email
                && r.reservationDatetime === reservation.reservationDatetime
                && r.restaurant === reservation.restaurant.name);

            if (existing) {
                existing.items.push(reservation);
            }
            else {
                reservations.push({
                    id: reservation.id,
                    email: reservation.email,
                    restaurant: reservation.restaurant.name,
                    status: reservation.status,
                    reservationDatetime: reservation.reservationDatetime,
                    items: [reservation],
                });
            }

        }
        setReservations(reservations);
    }

    async function setReservationStatus(reservations, status) {
        reservations.items.forEach(r => r.status = status);
        let data = JSON.stringify(reservations.items);
        console.log(data);
        const response = await fetch('http://localhost:8081/reservation', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'token'
            },
            body: data,
        });
        let results = await response.json();

        if (response.status < 400) {
            try {
                await mailslurp.inboxController.sendEmailAndConfirm(
                    "d21809fe-f12b-4359-8a8e-686b419680b9",
                    {
                        to: [`${reservations.email}`],
                        subject: "Reseveration receive",
                        body: `Your Reservation is ${status}`,
                    },
                );
            } catch (err) {

            }
            getReservations();
        }
    }

    useEffect(() => {
        if (!reservationsLoaded.current) {
            getReservations();
            reservationsLoaded.current = true;
        }

    });

    function handleLogOut() {
        window.location.href = '/'
    }

    return (
        <div className="container-fliud h-100">
            <MainNavigation />
            <div className="shadow-full bg-white h-85 overflow-auto my-3 mx-auto p-3">
                <div className="d-flex justify-content-end">
                    <button type="button" className="btn btn-right" onClick={(e) => handleLogOut(e)}>LogOut</button>
                </div>
                <div className="d-flex flex-column">
                    <table className={"table"}>
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Email</th>
                                <th scope="col">Restaurant</th>
                                <th scope="col">Tables requested</th>
                                <th scope="col">Date</th>
                                <th scope="col" colSpan='2'>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {reservations.map((reservation, index) =>
                                <tr key={"table" + index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{reservation.email}</td>
                                    <td>{reservation.restaurant}</td>
                                    <td>{reservation.items.length}</td>
                                    <td>{reservation.reservationDatetime}</td>
                                    <td>{reservation.status}</td>
                                    <td>
                                        {(reservation.status === 'pending' &&
                                            <div className="btn-group">
                                                <button className="btn btn-primary" onClick={() => setReservationStatus(reservation, 'approved')}>Approve</button>
                                                <button className="btn btn-outline-primary" onClick={() => setReservationStatus(reservation, 'denied')}>Deny</button>
                                            </div>) ||
                                            (reservation.status === 'approved' && <span className="border bg-success text-white p-2">Approved</span>) ||
                                            (<span className="border bg-secondary text-white p-2">Denied</span>)
                                        }
                                    </td>
                                </tr>
                            )}

                        </tbody>
                    </table>
                </div>
            </div>
            <Footer />
        </div>
    );
}