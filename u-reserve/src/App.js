import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './site.css'
import Home from './pages/home';
import Contact from './pages/contact';
import Restaurant from './pages/restaurant';
import ReserveTables from './pages/reserve';
import Business from './pages/business';
import AddTable from './pages/addtable';
import AdministratorLogin from './pages/administratorlogin';
import Administration from './pages/administration';


export default function App() {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/contact' component={Contact} />
            <Route exact path='/restaurant' component={Restaurant} />
            <Route exact path='/adminlogin' component={AdministratorLogin} />
            <Route exact path='/reserve' component={ReserveTables} />
            <Route exact path='/business' component={Business} />
            <Route exact path='/addtable' component={AddTable} />
            <Route exact path='/administration' component={Administration} />
        </Switch>
    );
}