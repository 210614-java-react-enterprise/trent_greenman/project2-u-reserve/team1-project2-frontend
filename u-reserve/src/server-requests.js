function sendAjaxRequest(method, url, body, requestHeaders, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    for(let requestHeaderKey in requestHeaders){
        xhr.setRequestHeader(requestHeaderKey, requestHeaders[requestHeaderKey]);
    }
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            console.log(xhr);
            if(xhr.status > 199 && xhr.status < 300){
                successCallback(xhr);
            } else{
                failureCallback(xhr);
            }
        }
    };
    if(body){
        xhr.send(body);
    }else{
        xhr.send();
    }
}

function sendAjaxGet(url, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("GET", url, null, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPost(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("POST", url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreatReservation(body, authToken, successCallback, failurecallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://localhost:8081/reservation"
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxGetReservations(authToken, successCallback, failureCallback){
    let requestUrl = "http://localhost:8081/reservation";
    let requestHeaders = {"Authorization":authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded"};
    let requestUrl = "http://localhost:8081/login";
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}